import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { HttpUtility } from './http.service';

@NgModule({
	imports: [HttpModule],
	providers: [HttpUtility],
})
export class RLHttpModule {}
