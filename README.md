A simple wrapper for angular's http service

Requires @angular/core and @angular/http

## Installation
Install rl-http from npm:
```
npm install rl-http --save
```
Configure with Angular-CLI and SystemJs:
```
/** Map relative paths to URLs. */
const map: any = {
    'rl-http': 'vendor/rl-http'
};

/** User packages configuration. */
const packages: any = {
    'rl-http': {
        main: 'index.js'
    }
};
```
Modify angular-cli-build.js by adding this line to vendorNpmFiles:
```
'rl-http/**/*.+(js|js.map)'
```

## Usage
The rl-http utility is meant be used as a substitute for angular's http. The provider is made available by importing the RLHttpModule into your main app module
```
import { RLHttpModule } from 'rl-http';

// module definition
imports: [RLHttpModule],
```
Services can inject his to make get, put, post, and delete requests
```
constructor(private http: HttpUtility) {}

myFunc(): void {
    this.http.get('/api/test', { search: 'what' });         // GET against /api/test?search=what
    this.http.post('api/test', testObj);                    // POST request against /api/test with testObj as the body
    this.http.put('/api/test/1', testObj);                  // PUT request against /api/test/1 with testObj as the body
    this.http.delete('/api/test/1', { softDelete: true });  // DELETE request against /api/test/1?softDelete=true
}
```
The rl-http utility handles the details of building the params object into a URLSearchParams object to provide to angular's http. It also builds a header to specify the content type as application/json. Finally, it jsonizes the body of PUT and POST requests, and parses the response of any request from JSON. These are things that angular's http service doesn't handle internally.

### Interceptor
In addition to making http requests easier to manage, the rl-http utility also provides the ability to specify an http interceptor. This interceptor runs when each http request completes to allow you to specify default success/error behavior. This is particularly useful for providing default behavior for handling certain http status codes.
To use the interceptor, you have to specify a provider for the abstract class HttpInterceptor. This will then get picked up by the rl-http utility and used for all requests.
```
import { HttpInterceptor } from 'rl-http';

class MyInterceptor extends HttpInterceptor {
    // optional
    handleSuccess(response: Response): any {
        if (response.status == 204) {
            console.log('No body to display');
        }
    }
    
    // optional
    handleError(response: Response): Observable<any> {
        if (response.status == 401) {
            login();
        }
    }
}

// provider
provide(HttpInterceptor, {
   useClass: MyInterceptor, 
}),
```
Note that the interceptor functions run over the raw http response, not the parsed data that the rl-http service returns. This gives the interceptor access to the http status codes.

## Tests
This project uses systemjs and karma to run unit tests.
```
npm install
npm run build
npm test
```
Alternately:
```
npm install
npm run build-test
```
If you encounter an issue and want to debug it:
```
npm run test.debug
```
or
```
npm run build-test.watch
```
(Runs tsc in watch mode and concurrently runs the test debugger)